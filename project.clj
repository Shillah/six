(defproject six "0.1.0"
  :type program
  :license "NONE"
  :url "http://example.com/six"
  :run
   {main {:file [src "main.pl"], :goal main/main, :args? true},
    test-1 {:file [test "test.pl"], :goal test_me, :args? false}}
  :vendors [swipl]
  :dependencies [[gitlab.Shillah/five "a99c3ce9"]])
