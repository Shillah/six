% -*- mode: prolog -*-
% This is your main file

:- module(six, [six/1]).

:- use_module(library(five/five), [five/1]).

six(X) :- five(Y), X is Y+1.
